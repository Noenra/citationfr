-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Dylan Benchalal--Galopin
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-22 16:59
-- Created:       2022-02-21 09:39
PRAGMA foreign_keys = OFF;

-- Schema: mydb
BEGIN;
CREATE TABLE "auteurs"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(35) NOT NULL,
  "Created-At" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "Modified-At" DATETIME,
  "Delated-At" DATETIME
);
CREATE TABLE "citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" VARCHAR(255) NOT NULL,
  "created-At" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "modified-At" DATETIME,
  "deleted-At" DATETIME,
  "auteurs_id" INTEGER NOT NULL,
  CONSTRAINT "fk_citations_auteurs"
    FOREIGN KEY("auteurs_id")
    REFERENCES "auteurs"("id")
);
CREATE INDEX "citations.fk_citations_auteurs_idx" ON "citations" ("auteurs_id");
COMMIT;
