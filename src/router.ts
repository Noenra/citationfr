import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CitationController from "./controllers/CitationController";
import AuteurController from "./controllers/AuteursController";
import CitationAuteurController from "./controllers/CitationAuteurController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.home(req, res);
    });
    app.get('/citations', (req, res) =>
    {
        CitationController.viewCitations(req, res);
    });
    app.post('/citations', (req, res) =>
    {
        CitationController.createCitation(req, res);
    });
    app.get('/auteurs', (req, res) =>
    {
        AuteurController.ViewAuteurs(req, res);
    });
    app.post('/auteurs', (req, res) =>
    {
        AuteurController.createAuteur(req, res);
    });
    app.get('/delete/:id', (req, res) =>
    {
        CitationAuteurController.deleteAuteur(req, res);
    });
    app.get('/delete_citation/:id', (req, res) =>
    {
        CitationAuteurController.deleteCitation(req, res);
    });
    app.get('/auteurs/:id', (req, res) =>
    {
        CitationAuteurController.ViewCitationsAuteurs(req, res);
    });

}
