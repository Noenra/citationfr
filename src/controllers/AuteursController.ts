import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class AuteurController
{
    static ViewAuteurs(req: Request, res: Response): void
    {
        const view_auteur = db.prepare(`SELECT * FROM auteurs`).all()
        res.render('pages/Auteurs', {
            title: 'AUTEURS',
            auteurs: view_auteur,
        });
    }
    static createAuteur(req: Request, res: Response): void
    {
        const create_auteur = db.prepare(`INSERT INTO auteurs ("name") VALUES (?)`).run(req.body.CreateName);
        AuteurController.ViewAuteurs(req, res);
    }

}
