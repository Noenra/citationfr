import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController
{
    static home(req: Request, res: Response): void
    {
        // const lastCitation = db.prepare(`SELECT * FROM citations WHERE ID = (SELECT MAX FROM citations)`)
        res.render('pages/Home', {
            title: 'HOME',
            // last_citation: lastCitation,
        });
    }
}