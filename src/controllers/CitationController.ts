import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class CitationController
{
    static viewCitations(req: Request, res: Response): void
    {
        const view_citation = db.prepare(`SELECT * FROM citations`).all()
        const view_auteur = db.prepare(`SELECT * FROM auteurs`).all()
        res.render('pages/Citations', {
            title: 'CITATIONS',
            citation: view_citation,
            auteur: view_auteur,
        });
    }

    static createCitation(req: Request, res: Response): void
    {
        console.log(req.body);
        
        const create_citation = db.prepare('INSERT INTO citations ("content", "auteurs_id") VALUES (?, ?)').run(req.body.content, +req.body.auteur_id);
        CitationController.viewCitations(req, res);
    }
}