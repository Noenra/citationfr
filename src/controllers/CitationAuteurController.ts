import { Request, Response } from "express-serve-static-core";
import { db } from "../server";
import AuteurController from "./AuteursController";
import CitationController from "./CitationController";

export default class CitationAuteurController
{
    static ViewCitationsAuteurs(req: Request, res: Response): void
    {
        const selectAuteur = db.prepare(`SELECT * FROM auteurs WHERE id = ?`).get(req.params.id);
        const selectCitation = db.prepare(`SELECT * FROM citations WHERE auteurs_id = ?`).get(req.params.id);
        res.render('pages/CitationsAuteur', {
            auteur: selectAuteur,
            citation: selectCitation,
        });
    }

    static editAuteur(req: Request, res: Response): void
    {
        const edit_auteur = db.prepare(`UPDATE auteurs SET name = ? WhERE id = ?`).run(req.body.EditName, req.params.id);
        CitationAuteurController.ViewCitationsAuteurs(req, res);
    }
    
    static deleteAuteur(req: Request, res: Response): void
    {
        db.prepare("DELETE FROM citations WHERE auteurs_id=?").run(req.params.id)
        db.prepare("DELETE FROM auteurs WHERE id=?").run(req.params.id);
        
        AuteurController.ViewAuteurs(req, res);
    }

    static deleteCitation(req: Request, res: Response): void
    {
        db.prepare("DELETE FROM citations WHERE id = ?").run(req.params.id);
        
        CitationController.viewCitations(req, res);
    }
}